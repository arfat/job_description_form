import TetherComponent from 'react-tether'
import React,{Component} from 'react';
import {ListItem} from 'material-ui/List';
import './Sample.css';
import Paper from 'material-ui/Paper';
import FontIcon from 'material-ui/FontIcon';
import Avatar from 'material-ui/Avatar';
import ActionInfo from 'material-ui/svg-icons/action/info';


class SimpleDemo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
    }
  }

  render() {

    const { isOpen } = this.state

    return(
      <TetherComponent
        attachment="top left"
        constraints={[{
          to: 'scrollParent',
          attachment: 'together',
        }]}
        style={{zIndex:'10',borderRadius:'10px'}}
      >

        <ListItem leftAvatar={<Avatar icon={<ActionInfo/>} size={15} style={{top:'20px'}}/>} style={{borderRadius:'20px'}} primaryText={this.props.title}onMouseOver={() => {this.setState({isOpen: !isOpen})}}  onMouseOut={() => {this.setState({isOpen: !isOpen})}} />
        {
          isOpen &&
          <Paper>
            <div class="message-body"
            style={{
            width:'350px',
            height:'300px',
            backgroundColor:'#EFEFEF',
            borderRadius:'10px 10px 10px 10px',
            border:'5px solid #54847B',
            display:'table-cell',
            opacity:'1.0'
            }}
            >
            <h2 style={{fontSize:'20px',textAlign:'center',color:'#211d1d',fontWeight:'bold',fontFamily:"'Times New Roman', Times, serif",marginTop:'10px'}}>{this.props.heading}</h2>
            <p style={{color:'#000000',textAlign:'left',padding:'10px'}}>{this.props.disc}</p>

            </div>
            </Paper>
        }
      </TetherComponent>
    )
  }
}

export default SimpleDemo;
