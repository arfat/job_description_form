import React,{Component} from 'react';
import Cookies from 'js-cookie';
import axios from 'axios';
import TinyMCE from 'react-tinymce';
import './App.css';
import './index.css';
import {List} from 'material-ui/List';
import SimpleDemo from './SimpleDemo';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import tyback from './images/tinymcuback.jpg';
import TextField from 'material-ui/TextField';




const token = Cookies.get('csrftoken');
class MainPage extends Component{
  constructor(props) {
    super(props);
    var input = localStorage.getItem("inputText")
    if(!input){input="<p style="+"color:red;"+">Hello</p>";}
    this.state = {
      value:input,
      value3:{},
      value2:{},
      message:{"Need more 'You' Statements" : "Strong listings balance `we` statements that describe your company and ‘you’ statements that directly address job seekers. Your listing needs more ‘you’ statements that tell applicants what you're looking for.Your listing needs more content that directly addresses job seekers. Try including more ‘you’ phrases such as “you care” and “your passion.” Listings with strong ‘you’ phrases have higher engagement.",
      				"Need more 'We' Statements" : "Strong listings balance `we` statements that describe your company and ‘you’ statements that directly address job seekers. Your listing needs more ‘we’ statements that tell applicants what you're looking for.Your job listing is competing for the attention of the market's best candidates. With so many potential jobs to check out, some people don't take listings seriously unless they contain enough detail to really understand the job. Job listings that are shorter than 300 words see a significant drop-off in engagement.",
      				"Too much directive language" : "Your job listing using directive language try to use some of indirective language.",
              "Used Candidate Language" : "Using formal language (like 'Candidate')  tends to attract less applications",
      				"Used Applicant Language" : "Use of words like 'applicant' tends to attract less applications",
              "Could use more engaging questions" : "Job listings that use a few questions to engage job seekers perform better. Add some questions to get more people to apply for your listing.\nYour message doesn‘t have enough questions. The best listings contain no more than 3-4. Try adding some to get more people to apply for your job.",
              "Contains too many questions" : "Job listings that use a few questions to engage job seekers perform better. Add some questions to get more people to apply for your listing.",
      				"Use equal opportunity statement" :"Your job listing does not contain an equal opportunity statement that asserts your company's commitment to fair hiring practices. This narrows the appeal of your posting and drives down engagement.",
      				"Need more bulleted content" : "The most successful job postings use bulleted lists for about a third of their content. Your listing does not contain enough bulleted content.",
              "Too much bulleted content" : "The most successful job postings use bulleted lists for about a third of their content. Your listing contains too much  bulleted content.",
              "Your sentences contain less words" : "Uses more no.of words for a sentence. Job seekers respond best to listings with sentences that average between 13-17 words.",
              "Your sentences contain more words" : "Uses less no.of words for a sentence. Job seekers respond best to listings with sentences that average between 13-17 words.",
      				"Listing is a bit short" : "Your listing is a bit  short. Ideal job listings are 600-700 words.",
      				"Listing is a bit long" : "Your listing is a bit  long. Ideal job listings are 600-700 words.",
      				"Listing is long" : "Your listing is a bit  long. Ideal job listings are 600-700 words.",
      				"Listing is short" : "Your listing is a bit  long. Ideal job listings are 600-700 words.",
          },
      value_input:1,
      document_color:{1:'white',2:'red'},
    };
      this.Documentchangecontent=this.Documentchange.bind(this);
  }
  handleChange = (event) => {
    const inputText=event.target.getContent();
    this.setState({sample:inputText});
    if(typeof(Storage)!=="undefined")
    {
      localStorage.setItem("inputText",inputText);
    }
    axios.post('.',{'inputText':inputText},{headers:{'X-CSRFToken':token}}).then(function (response) {
      console.log(response)
   this.setState({value2: response.data.data1,value3: response.data.data2})
 }.bind(this))
.catch(function (error) {
   console.log(error);
});
};

 Documentchange(event)
 {
   const value=event.target.id();
   this.setState({value_input:value});
 };

  render(){
    const {value2,value3,message} = this.state;
    let output_value =[];
    let output2_value=[];
    let phrases_list=[];
    console.log(output_value);
    if(value2){
      output_value = Object.keys(value2).map(function(key){
          if(key && value2[key] && message[value2[key]])
          {
            return(
                <SimpleDemo
                title={value2[key]}
                heading={value2[key]}
                disc={message[value2[key]]}
                />
              );
          }
          else{
            return null;
          }
        });
      }


    if(value3)
    {
        output2_value = Object.keys(value3).map(function(key){
            if(value3[key])
            {
                return(
                  <SimpleDemo
                  title={value3[key]["title"]}
                  heading={value3[key]["heading"]}
                  disc={value3[key]["disc"]}
                  />
                );
              }
              else{
                return null;
              }
            }
          );
    }
    //     phrases_list = Object.keys(value3).map(function(key)
    //       {
    //       if(key==="repititive_phrases")
    //         {
    //         var ree = value3[key]["disc"].split("\',\'");
    //         return ree;
    //         }
    //       else
    //       {
    //         var kk = [];
    //         return kk;
    //       }
    //       }
    //     );
    //     console.log(phrases_list);
    //     var i;
    //     for(i=0;i<phrases_list.length;i++)
    //     {
    //       if(phrases_list[i].length>0)
    //       {
    //         for(var key in phrases_list[i])
    //         {
    //           var my_text = String(sample);
    //           console.log(my_text);
    //           var substring = phrases_list[i][key];
    //            my_text = my_text.includes(substring,"<p style="+"color:'red';"+substring+"</p>");
    //            console.log(my_text);
    //         }
    //       }
    //     }
    // }

  return(
    <div style={{'display':'flex'}}>
    <div className="Text">
    <div className="HeaderBar">
    <TextField style={{width:'300px',position:'absolute',marginLeft:'10px',fontSize:'30px'}}
    hintText="Effective name"
    floatingLabelText="Job Description Name"
    floatingLabelStyle={{fontSize:'20px',color:'#756177'}}
    hintStyle={{fontSize:'20px'}}
    />
    <SelectField floatingLabelText="New Document" value="Job recruiting" style={{width:'200px',marginLeft:'450px'}}>
              <MenuItem value="Job recruiting" primaryText="Job recruiting"/>
              <MenuItem value="Email recruiting" primaryText="Email recruiting"/>
    </SelectField>
    </div>
    <TinyMCE
      content={this.state.value}

      config={{
        content_style : '.my_tinymce_body{  font-size:20px;font-family:sans-serif;background-color:'+this.state.document_color[this.state.value_input]+'}.',
        body_class : "my_tinymce_body",
        content_style : '.my_tinymce_body{ background-image:url("'+tyback+'");background-image:no-repeat;font-size : 20px; font-family:sans-serif ;  }.',
        plugins: 'autolink link image lists print preview spellchecker textcolor',
        toolbar: 'undo redo | bold italic | alignleft | bullist numlist | spellchecker',
        toolbar_items_size : 'large ',
        editor_css: "../node_modules/tinymce/skins/lightgray/skin.min.css",
        menubar: false,
        height:'500px',
        skin:'lightgray',
        width:'800px',
        preview_styles:'font-size:30px',
        themes:'modern',
        browser_spellcheck: true,
        elementpath: false
      }}
      onChange={this.handleChange}
    />
    </div>
    <div style={{paddingLeft:'2%'}}>
    <List style={{
      position:'absolute',
      top:'10%',
      right:'50px',
      margin:'auto',
      backgroundColor: 'white',
      marginBottom:'5px',
      borderRadius:'20px'
      }} >
        {output_value}
        {output2_value}
    </List>
    </div>
    </div>
    );
  }
}
export default MainPage;
