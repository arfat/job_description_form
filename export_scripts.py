import re, glob, os
import shutil
def renamer(name,path):
    os.chdir(path)
    files="*."+name
    pattern="^(.*)\.%s$"%(name)
    replacement="jd_score."+name
    for pathname in glob.glob(files):
        basename= os.path.basename(pathname)
        new_filename= re.sub(pattern, replacement, basename)
        if new_filename != basename:
            os.rename(
              pathname,
              os.path.join(os.path.dirname(pathname), new_filename))


renamer("css.map","/home/sundar/my-react-app/build/static/css")
renamer("css","/home/sundar/my-react-app/build/static/css")
renamer("js","/home/sundar/my-react-app/build/static/js")
renamer("js.map","/home/sundar/my-react-app/build/static/js")
os.chdir('/home/sundar/my-react-app/build/static/')
from distutils.dir_util import copy_tree
copy_tree('css','/home/sundar/JD/score/static/css')
copy_tree('js','/home/sundar/JD/score/static/js')
print "Successfully js,css copied"


src_files = os.listdir("/home/sundar/my-react-app/build/static/media")
for file_name in src_files:
    full_file_name = os.path.join("/home/sundar/my-react-app/build/static/media",file_name)
    if (os.path.isfile(full_file_name)):
        shutil.copy(full_file_name, "/home/sundar/JD/score/static/media")

print "Media file copied Successfully copied"

